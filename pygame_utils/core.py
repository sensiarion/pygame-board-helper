import os
import random
from abc import abstractmethod, ABC
from collections import defaultdict
from typing import Callable, List, Dict, Tuple, Optional, Type, Literal

import pygame


def load_image(path: str, directory: str = 'assets',
               size: Optional[Tuple[int, int]] = None) -> pygame.Surface:
    """
    Загружает изображение
    :param path: путь до изображения
    :param directory: путь до папки с изображением. Если установлена в **None**
    то для изображения будет использован абсолютный путь
    :param size: размер изображения
    :return: полотно с изображением
    """
    full_path = os.path.join(directory, path) if directory is not None else path
    if not os.path.isfile(full_path):
        raise FileNotFoundError(f"Unable to find path to image: {full_path}")

    image = pygame.image.load(full_path)
    if size is not None:
        image = pygame.transform.scale(image, size)

    return image


class RenderableObject(ABC):
    """
    Абстракный класс для описание минимального элемента, который может быть отображён на экране
    """

    @abstractmethod
    def render(self, screen: pygame.Surface, time_delta: int):
        """
        Выполняется каждрый раз при отрисовке кадра.

        Его необходимо переопределить
        :param screen: полотно для отрисовки
        :param time_delta: время от последнего кадра в миллисекундах
        :return:
        """
        pass

    @abstractmethod
    def setup(self, game: 'Game'):
        """
        Вызывается при создании объекта
        :param game: полотно для отрисовки
        """
        pass

    @abstractmethod
    def update(self, game: 'Game'):
        """
        Итерация игры, обновляющая состояние объекта
        :param game:
        :return:
        """
        pass


class ImageObject(RenderableObject, ABC):
    """
    Класс для отрисовки изображения
    """

    def __init__(self, path: str, coords: Tuple[int, int], directory='assets', size: Tuple[int, int] = None):
        """
        :param path: путь до файла
        :param coords: координаты для отрисовки изображения
        :param directory: папка, в которой находятся изображения
        :param size: размер изображения
        """
        super().__init__()

        self.size = size
        self.directory = directory
        self.path = path

        self.image = load_image(path, directory, size)
        self.rect = pygame.Rect(coords[0], coords[1], *self.image.get_size())

    def render(self, screen: pygame.Surface, time_delta: int):
        screen.blit(self.image, self.rect)


class SpriteGroup(RenderableObject, pygame.sprite.Group):
    """
    Класс для отрисовки группы спрайтов. Сами по себе спрайты недоступны для отрисовки и
    подлежат группировке в набор объектов, который уже может быть отрисован
    """

    def setup(self, game: 'Game'):
        pass

    def update(self, game: 'Game'):
        pygame.sprite.Group.update(self, game)

    def render(self, screen: pygame.Surface, time_delta: int):
        self.draw(screen)


class SpriteObject(pygame.sprite.Sprite):
    """
    Класс для работы со спрайтом. Любой спрайт ассоциируются с некоторым изображением,
    поэтому для урпощения жизни были добавлены параметры для создания изображения вместе с спрайтом
    """

    def __init__(self, image_path: str, coords: Tuple[int, int], image_dir: str = 'assets',
                 size: Tuple[int, int] = None, *groups: pygame.sprite.AbstractGroup):
        super().__init__(*groups)
        self.image_dir = image_dir
        self.image_path = image_path

        self.image = load_image(image_path, image_dir, size)
        self.rect = pygame.Rect(coords[0], coords[1], *self.image.get_size())

    def update(self, game: 'Game') -> None:
        pass


class AnimatedSprite(pygame.sprite.Sprite):
    """
    Простенький класс для анимированных спрайтов.

    У каждого анимиранного спрайта может быть набор определённых действий, по которым могут запускаться
    анимации.

    Как минимум, у каждого объекта должен быть набор спрайтов для действия **idle** (простой). Который
    проигрывается по умолчанию. Все остальные анимации необходимо запускать самостоятельно, при каких-либо
    действиях в игре
    """
    AvailableActions = Literal['idle', 'walking', 'falling']

    def __init__(self, image_paths: Dict[AvailableActions, List[str]],
                 coords: Tuple[int, int], image_dir: str = 'assets',
                 size: Tuple[int, int] = None,
                 current_action: AvailableActions = 'idle',
                 *groups: pygame.sprite.AbstractGroup):
        super().__init__(*groups)

        if len(image_paths.get('idle', [])) == 0:
            raise ValueError("At least 1 sprite should be specified for animated sprite")

        self.action_sprites: Dict[str, List[pygame.Surface]] = dict()
        # подгружаем изображения из спрайтов
        for action, paths in image_paths.items():
            images: List[pygame.Surface] = []
            for sprite_path in paths:
                images.append(load_image(sprite_path, image_dir, size))

            self.action_sprites[action] = images

        self.__speed = 1.0
        self.__started = False
        self.__counter = 0
        self._index = 0
        self.current_action = current_action

        self.image = self.action_sprites[self.current_action][self._index]
        self.rect = pygame.Rect(coords[0], coords[1], *self.image.get_size())

    def start(self, action: AvailableActions = 'idle', speed: float = 1):
        """
        Начинает воспроизводить анимацию
        :param action: действие, по которому будет начат анимация
        :param speed: скорость воспроизведения анимации
        :return:
        """
        self.current_action = action
        self.__started = True
        self.__speed = speed * 0.1
        self._index = 0

    def stop(self):
        """
        Останавливает воспроизведение анимации
        """
        self.__started = False

    def is_started(self):
        """
        Проверка на то, запущена ли анимация
        :return:
        """
        return self.__started

    def update(self, game: 'Game') -> None:
        """
        Простенький вариант для работы анимации
        """
        if self.__started:
            # увеличиваем счётчик на каждой итерации на некотое небольшое значение
            self.__counter += self.__counter + 1 * self.__speed
            if self.__counter >= 1:
                # если счётчик дошёл до отмечки, то выставляем следующее в списке изображение на отрисовку
                self._index = (self._index + 1) % len(self.action_sprites[self.current_action])
                self.__counter = 0

            self.image = self.action_sprites[self.current_action][self._index]


class Game(ABC):
    """
    Основной класс для игры
    """
    screen: pygame.Surface

    GRAVITY = 4

    def __init__(self, width: int = 600, height: int = 400, name: str = 'Game', fps: int = 60):
        """
        Размеры окна игры
        :param width: ширина окна
        :param height: высота окна
        :param name: название для окна
        :param fps: кол-во кадров в секунду в окне
        """
        self.fps = fps
        self.name = name
        self.height = height
        self.width = width

        self.clock = pygame.time.Clock()

        self._running = False

        self.__objects: List[RenderableObject] = []
        self.__groups: List[SpriteGroup] = []

        annotation = Callable[[pygame.event.Event], None]
        self._handlers: Dict[int, List[annotation]] = defaultdict(list)
        self.setup()

    @abstractmethod
    def setup(self):
        """
        Метод, выполняемый при инициализации объекта игры.

        В нём можно создавать внутрениие объекты игры и инициализировать все необходимые
        для начала игры переменные
        """
        pass

    @abstractmethod
    def draw(self, time_delta: int):
        """
        Метод по отрисовке окна игры.

        Вызывается каждый раз перед отрисовкой объектов игры
        :param time_delta: время в миллисекндах с отрисовки предыдущего кадра
        """
        pass

    def update(self):
        """
        Обновляет состояние всех игорвых объектов
        """
        for obj in self.__objects:
            obj.update(self)

    def is_running(self) -> bool:
        """
        Проверка на то, запущенна ли игра
        """
        return self._running

    def _draw(self, time_delta: int):
        for object in self.__objects:
            object.render(self.screen, time_delta)

    def add_object(self, obj: RenderableObject):
        """
        Добавляет объект для отрисовки на экран.

        Должен быть экземпляром класса или наследника класса RenderableObject
        :param obj: созданный объект для отрисовки
        """
        obj.setup(self)
        self.__objects.append(obj)
        if isinstance(obj, SpriteGroup):
            self.__groups.append(obj)

    def get_objects(self) -> List[RenderableObject]:
        return self.__objects

    def get_sprite_groups(self) -> List[SpriteGroup]:
        return self.__groups

    def add_handler(self, event_type: int, handler: Callable):
        """
        Добавляет функцию обработчик для действия.

        Обработчик будет запущен при наступлении указанного события
        :param event_type: константна события, по которой можно опознать событие (например pygame.MOUSEBUTTONDOWN)
        :param handler: функция обработчик, которой будет передан аргумент с событием
        :return:
        """

        self._handlers[event_type].append(handler)

    def clear(self) -> Tuple[List[RenderableObject], List[SpriteGroup]]:
        """
        Удаляет все объекты для отрисовки в игре
        :return:
        """
        objects = self.__objects
        groups = self.__groups
        self.__objects = []
        self.__groups = []

        return objects, groups

    def set(self, objects: List[RenderableObject], groups: List[SpriteGroup]):
        """
        Устанавливает новые объекты для отрисовки
        :param objects:
        :param groups:
        :return:
        """
        self.__objects = objects
        self.__groups = groups

    def run(self):
        """
        Метод для запуска игры
        """
        # на самом деле данную функцию логичнее вызывать до запуска игры, т.к. она необходима для
        # корректной подгрузки шрифтов и аудио
        pygame.init()
        pygame.display.set_caption(self.name)
        self.screen = pygame.display.set_mode((self.width, self.height))
        self._running = True
        while self._running:
            # перед новой отрисовкой кадр очищается
            self.screen.fill((0, 0, 0))

            # перехват всех событий
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self._running = False
                for handler in self._handlers.get(event.type, []):
                    handler(event)

            time_delta = self.clock.tick(self.fps)

            # отрисовка базовых элементов игры и всех объектов в игре
            self.draw(time_delta)
            self._draw(time_delta)

            # обновление логики/объектов, находящихся в игре
            self.update()

            # подменяем изображение (двойная буфферизация)
            pygame.display.flip()
        pygame.quit()


class Text(RenderableObject):
    """
    Обёртка вокруг текста для более удобной отрисовке на экране
    """

    def __init__(self, text, pos: Tuple[int, int], font_size=20, color=(255, 255, 0)):
        """
        :param text: стартовый текст для отрисовки
        :param pos: кортеж с координатами верхнего левого угла текста
        :param font_size: размер шрифта
        :param color: цвет для отрисовки
        """
        self.color = color
        self.font_size = font_size
        self.pos = pos
        self.__text = text
        self.__freeze = False

    def render(self, screen: pygame.Surface, time_delta: int):
        screen.blit(self.text_surface, self.pos)

    def freeze(self):
        """
        Замораживает текст, запрещая его изменять до момента разморозки
        """
        self.__freeze = True

    def unfreeze(self):
        """
        Снимает заморозку текста, позволяя его изменять
        :return:
        """
        self.__freeze = False

    def set_text(self, text: str):
        """
        Назначает текст для отрисовки
        :param text: текст, который будет отображён на экране
        """
        if self.__freeze:
            return
        self.__text = text

        self.font = pygame.font.SysFont('Roboto', self.font_size)
        self.text_surface = self.font.render(self.__text, False, self.color)

    def setup(self, game: 'Game'):
        """
        Инициализация текста для отрисовки
        :param game: экземпляр игры
        """
        self.font = pygame.font.SysFont('Roboto', self.font_size)
        self.text_surface = self.font.render(self.__text, False, self.color)


class PhysicalSprite(pygame.sprite.Sprite, ABC):
    """
    Спрайт подчиняющийся физике, логика которой будет указана в методе расчёта
    """

    def __init__(self, *groups: pygame.sprite.AbstractGroup):
        super().__init__(*groups)
        self.previously_collided = set()
        self.current_collided = set()

    @abstractmethod
    def _calc(self, game: Game):
        """
        Выполняет расчёты связанные с физикой данного объекта
        :param game: экземпляр игры в котором находится объект
        :return:
        """
        pass

    def update(self, game: Game) -> None:
        """
        Расчёт физики для объектов.
        У всех физических (унаследованных от данного класса) объектов будет вызван calc.
        Все иные объекты будут проигнорированны
        :param game: экземпляр игры
        """
        collision = False
        for group in game.get_sprite_groups():
            if collided := pygame.sprite.spritecollideany(self, group):
                # обработку столкновений необходимо делать только для объектов, у которых описана физика
                if not isinstance(collided, PhysicalSprite):
                    continue
                if collided is not self:
                    collision = True
                    self.current_collided.add(collided)

                    # noinspection PyTypeChecker
                    self.on_collision(collided, game)

        # при столкновении с препятствием обработка физики не происходит
        # (никто не мешает вам в таком случае сделать отдельный обработчик, описывающий столкновения)
        if not collision:
            self._calc(game)

        # для отдельной обработки первого столкновения находим такие объекты
        # первым столкновениям считаются все столкновения, которые не были совершены на предыдущей
        # итерации обновления
        if new_collieded := self.current_collided - self.previously_collided:
            for collided in new_collieded:
                self.on_first_collision(collided, game)

        # подменяем буферы, отвечающие за хранение столкнувшихся объектов
        self.previously_collided.clear()
        self.previously_collided.update(self.current_collided)
        self.current_collided.clear()

    def on_collision(self, collided_sprite: SpriteObject, game: Game):
        """
        Функция, выполняемая при столкновении с объектом
        :param collided_sprite: объект с которым произошло столкновение
        """
        pass

    def on_first_collision(self, collided_sprite: SpriteObject, game: Game):
        """
        Функция, выполняемая при первом столконевении с объектом.

        Под первым столкновением понимаются все стоклновения, произошедшие с разрывом коллизии хотя бы на 1
        итерации
        :param collided_sprite: объект с которым произошло столкновение
        :return:
        """


class AbstractParticle(pygame.sprite.Sprite):
    """
    Класс для работы с частицами.

    Каждая создаваемая частица подчиняется законам физики (очень похоже на реальную физику).

    **у каждой частицы должен быть установлен массив изображений частиц** particles, в котором
    должны находится изображения частицы в разных масштабах
    """
    # набор частиц различного размера из которых будут выбираться частицы для анимации
    particles: List[pygame.Surface]

    def __init__(self, pos: Tuple[int, int], dx, dy, *groups: pygame.sprite.AbstractGroup):
        if not self.particles:
            raise ValueError("Необходимо указать изображения для частиц (как минимум 1)")

        super().__init__(*groups)
        self.dy = dy
        self.dx = dx
        self.post = pos

        # начальная скорость объектов
        self.velocity = [dx, dy]

        self.image = random.choice(self.particles)
        self.rect = self.image.get_rect()

        self.rect.x, self.rect.y, = pos

    def update(self, game: 'Game'):
        """
        При обновлении частицы она двигается по определённой траектории под действием силы тяжести
        :param game: экземпляр игры, в котором существует частицы
        """
        self.velocity[1] += game.GRAVITY * 0.1

        self.rect.x = int(self.rect.x + self.velocity[0])
        self.rect.y = int(self.rect.y + self.velocity[1])

        # все частицы, вылетевшие за экран уничтожаются
        # (ничего не мешает вам явно определить границы, после которых частица будет убираться с экрана)
        if not self.rect.colliderect(game.screen.get_rect()):
            self.kill()


def create_particles(partical_class: Type[AbstractParticle], pos: Tuple[int, int], count=20,
                     speed_range: Tuple[int, int] = (-5, 6)) -> SpriteGroup:
    """
    Функция помощник для создания частиц
    :param partical_class: класс, по которому создаётся частика
    :param pos: стартовая позииция для всех частиц
    :param count: кол-во частиц
    :param speed_range: диапазон скоростей, с которыми начнут двигаться частицы
    :return:
    """
    speeds = range(*speed_range)

    group = SpriteGroup()
    for _ in range(count):
        group.add(partical_class(pos, random.choice(speeds), random.choice(speeds)))

    return group
