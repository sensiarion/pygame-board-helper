import os
from abc import ABC
from typing import Tuple, Optional, List, Dict
import pygame_utils
import pygame

from pygame_utils.core import Game, SpriteObject, load_image, SpriteGroup, PhysicalSprite, AbstractParticle, \
    create_particles, AnimatedSprite


class Particle(AbstractParticle):
    particles = [load_image('floor.jpg', 'data', (15, 15))]
    for scale in (5, 7, 10):
        particles.append(pygame.transform.scale(particles[0], (scale, scale)))


class UsualPhysicalSprite(PhysicalSprite, ABC):

    def _calc(self, game: 'MyGame'):
        self.rect = self.rect.move(0, game.GRAVITY)


class MinotaurNpc(AnimatedSprite, UsualPhysicalSprite):

    def __init__(self, coords: Tuple[int, int], size: Tuple[int, int] = None,
                 current_action: AnimatedSprite.AvailableActions = 'idle',
                 *groups: pygame.sprite.AbstractGroup):
        sprite_map: Dict[str, List[str]] = dict()

        for action_folder in ('falling', 'idle', 'walking'):
            sprite_map[action_folder] = [f'assets/{action_folder}/' + i
                                         for i in os.listdir(f'assets/{action_folder}')]

        AnimatedSprite.__init__(self, sprite_map, coords, None, size, *groups,
                                current_action=current_action)
        UsualPhysicalSprite.__init__(self, *groups)

        self.direction: Optional[bool] = None
        self.rotated_sprites: Dict[str, List[pygame.Surface]] = dict()
        for action, images in self.action_sprites.items():
            rotated_images = [pygame.transform.flip(i, True, False) for i in self.action_sprites[action]]
            self.rotated_sprites[action] = rotated_images

        self.usual_sprites = self.action_sprites

        pygame.mixer.init()
        self.moving_sound = pygame.mixer.Sound('./data/footstep.mp3')
        self.moving_sound.set_volume(0.1)

    def move(self, event):
        if event.key == pygame.K_a:
            self.direction = False
            self.action_sprites = self.rotated_sprites
            self.start(action='walking')
            self.moving_sound.play(10)
        if event.key == pygame.K_d:
            self.direction = True
            self.action_sprites = self.usual_sprites
            self.start(action='walking')
            self.moving_sound.play(10)

    def stop_move(self, event):
        if event.key in (pygame.K_a, pygame.K_d):
            self.moving_sound.stop()
            self.direction = None
            self.start('idle')

    def update(self, *args, **kwargs) -> None:
        AnimatedSprite.update(self, *args, **kwargs)
        UsualPhysicalSprite.update(self, *args, **kwargs)
        if self.direction is not None:
            if self.direction:
                self.rect = self.rect.move(1, 0)
            else:
                self.rect = self.rect.move(-1, 0)

    def on_first_collision(self, collided_sprite: SpriteObject, game: 'MyGame'):
        self.start('idle')
        bottom_pos = (self.rect.x + self.rect.width // 2, self.rect.y + self.rect.h)
        particles = create_particles(Particle, bottom_pos, speed_range=(-10, 10))

        game.add_object(particles)


class Floor(SpriteObject, PhysicalSprite):

    def _calc(self, game: Game):
        pass


class MyGame(Game):
    GRAVITY = 4

    def setup(self):
        group = SpriteGroup()
        static = SpriteGroup()
        obj = MinotaurNpc((200, 200), size=(200, 200), current_action='falling')
        obj.start('falling')
        floor = Floor('floor.jpg', (0, self.height - 20), image_dir='data', size=(self.width, 100))
        group.add(obj)
        static.add(floor)

        self.add_handler(pygame.KEYDOWN, obj.move)
        self.add_handler(pygame.KEYUP, obj.stop_move)

        self.add_object(group)
        self.add_object(static)

    def draw(self, time_delta: int):
        pass


if __name__ == '__main__':
    game = MyGame(800, 600)
    game.run()
