import pygame

from pygame_utils.core import Game, load_image, SpriteGroup


class AnimatedSprite(pygame.sprite.Sprite):
    def __init__(self, sheet:pygame.Surface, columns, rows, x, y):
        super().__init__()
        self.frames = []
        self.cut_sheet(sheet, columns, rows)
        self.cur_frame = 0
        self.image = self.frames[self.cur_frame]
        self.rect = self.rect.move(x, y)

    def cut_sheet(self, sheet:pygame.Surface, columns, rows):
        self.rect = pygame.Rect(0, 0, sheet.get_width() // columns,
                                sheet.get_height() // rows)
        for j in range(rows):
            for i in range(columns):
                frame_location = (self.rect.w * i, self.rect.h * j)
                self.frames.append(sheet.subsurface(pygame.Rect(
                    frame_location, self.rect.size)))

    def update(self,game):
        self.cur_frame = (self.cur_frame + 1) % len(self.frames)
        self.image = self.frames[self.cur_frame]


class MyGame(Game):
    def setup(self):
        dragon = AnimatedSprite(load_image("pygame-8-1.png", 'data'), 8, 2, 50, 50)
        group = SpriteGroup()
        group.add(dragon)
        self.add_object(group)

    def draw(self, time_delta: int):
        pass


if __name__ == '__main__':
    game = MyGame(width=200,height=200,fps=60)
    game.run()
