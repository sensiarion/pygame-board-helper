import pygame

from pygame_utils.core import Game, AbstractParticle, load_image, create_particles


class StarParticle(AbstractParticle):
    particles = [load_image('star.png', 'data', (30, 30))]
    for size in (5, 10, 20):
        particles.append(pygame.transform.scale(particles[0], (size, size)))


class MyGame(Game):
    def setup(self):
        self.add_handler(pygame.MOUSEBUTTONDOWN,self.on_click)

    def draw(self, time_delta: int):
        pass

    def on_click(self, event):
        self.clear()
        self.add_object(create_particles(StarParticle, event.pos))


if __name__ == '__main__':
    game = MyGame()

    game.run()
